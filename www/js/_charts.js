var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40'];

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#chart_umrtnost').highcharts({

        title: {
            text: 'Úmrtí spojená s konzumací alkoholu'
        },

        subtitle: {
            text: 'Tradičně mají vysokou úmrtnost spojenou s alkoholem muži, u žen se během dvaceti let zdvojnásobila'
        },

        xAxis: {
            title: {
                text: ''
            }
        },

        yAxis: {
            title: {
                text: 'Podíl úmrtí (%)'
            }
        },

		tooltip: {
            formatter: function() {
                return 'Podíl úmrtí spojených s konzumací alkoholu na všech úmrtích: <b>' + this.y + ' %</b>'
            },
            crosshairs: true
        },

        legend: {
        },

        exporting: {
                    enabled: false
        },

        plotOptions: {
            series: {
                    compare: 'value',
                    lineWidth: 2,
                    marker: { symbol: 'circle' }
            }
        },

        credits: {
            href : 'http://casopis.adiktologie.cz/public.magazine/download-file/116',
            text : 'Zdroj: Odhad vlivu konzumace alkoholu na úmrtnost v České republice'
        },

        series: [{
            name: 'muži',
            data: [
            [1994, 10.3],
            [1995, 10.4],
            [1996, 10.3],
            [1997, 10.2],
            [1998, 10.6],
            [1999, 10.5],
            [2000, 10.4],
            [2001, 10.3],
            [2002, 10.3],
            [2003, 10.5],
            [2004, 10.0],
            [2005, 10.3],
            [2006, 10.1],
            [2007, 10.4],
            [2008, 10.3],
            [2009, 10.1],
            [2010, 10.2]
        ],
            color: colors[1],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[1]
            }
        }, {
            name: 'ženy',
            data: [
            [1994, 1.2],
            [1995, 1.3],
            [1996, 1.4],
            [1997, 1.5],
            [1998, 1.5],
            [1999, 1.4],
            [2000, 2],
            [2001, 2],
            [2002, 2],
            [2003, 2.1],
            [2004, 2.2],
            [2005, 2.3],
            [2006, 2.3],
            [2007, 2.3],
            [2008, 2.4],
            [2009, 2.2],
            [2010, 2.3]
        ],
            color: colors[0],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[0]
            }
        }]
    });
});

$(function () {

    var muzi = [['15-24',15.5], ['25-34',19.5], ['35-44',25.9], ['45-54', 23.4], ['55-64', 16.2], ['65-74', 9.8], ['75+', 4.4]]

    var zeny = [['15-24',11.2], ['25-34',11.2], ['35-44',17.4], ['45-54', 13.7], ['55-64', 8.2], ['65-74', 3], ['75+', 0.5]]

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#chart_vek').highcharts({

        title: {
            text: 'V jakém věku hrozí úmrtí spojené s alkoholem?'
        },

        subtitle: {
            text: 'Kritický věk je u mužů i žen mezi 35 a 44 lety'
        },

        xAxis: {
            title: {
                text: 'Věk'
            },
            tickInterval: 1,
            labels: {
                enabled: true,
                formatter: function() { return muzi[this.value][0];},
            }
        },

        yAxis: {
            title: {
                text: 'Podíl úmrtí (%)'
            }
        },

		tooltip: {
            formatter: function() {
                return 'Podíl úmrtí spojených s konzumací alkoholu na všech úmrtích v tomto věku: <b>' + this.y + ' %</b>'
            },
            crosshairs: true
        },

        legend: {
        },

        exporting: {
                    enabled: false
        },

        plotOptions: {
            series: {
                    compare: 'value',
                    lineWidth: 2,
                    marker: { symbol: 'circle' }
            }
        },

        credits: {
            href : "http://casopis.adiktologie.cz/public.magazine/download-file/116",
            text : "Zdroj: Odhad vlivu konzumace alkoholu na úmrtnost v České republice"
        },

        series: [{
            name: 'muži',
            data: muzi,
            color: colors[1],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[1]
            }
        }, {
            name: 'ženy',
            data: zeny,
            color: colors[0],
            marker: {
                fillColor: 'white',
                lineWidth: 2,
                lineColor: colors[0]
            }
        }]
    });
});

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: ' ',
                decimalPoint: ',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

    $('#chart_riziko').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Nárazové pití alkoholu'
        },
        subtitle: {
            text: 'Kdo vypije měsíčně alespoň při jedné příležitosti najednou nejméně tři půllitry piva nebo sklenice vína?'
        },
        xAxis: {
            categories: ['15-24', '25-34', '35-44', '45-54', '55-64', '65-74', '75+'],
            title: {
                text: 'Věk'
            }
        },
        yAxis: {
            title: {
                text: 'Podíl v populaci (%)'
            }
        },
        tooltip: {
            formatter: function() {
                return 'Ve věkové kategorii <b>' + this.x + '</b> nárazově pije <b>' + this.y + ' %</b> populace'
            },
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://www.uzis.cz/node/7495',
            text : 'Zdroj: Evropské výběrové šetření o zdraví 2014'
        },
        plotOptions: {
        },
        series: [{
            name: 'muži',
            data: [37.6, 33.4, 27.2, 23.8, 19.2, 15.8, 6.3],
            color: colors[1]
        },{
            name: 'ženy',
            data: [17.2, 15.5, 9.8, 8.1, 6.2, 3.6, 1.4],
            color: colors[0]
        }]
    });
});