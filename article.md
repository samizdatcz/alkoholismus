---
title: "Alkohol zabíjí dvakrát víc žen než před dvaceti lety"
perex: "Ještě v polovině devadesátých let měli na problémy s alkoholem monopol muži. Dnes je začínají dotahovat ženy. Kritický je věk 35 až 44 let."
description: "Ještě v polovině devadesátých let měli na problémy s alkoholem monopol muži. Dnes je začínají dotahovat ženy. Kritický je věk 35-44 let."
authors: ["Jan Boček"]
published: "19. ledna 2017"
coverimg: https://interaktivni.rozhlas.cz/data/alkoholismus/www/media/cover.jpg
coverimg_note: "Foto <a href='https://www.flickr.com/photos/c_h_p/6579782453/'>chrishelenphoto | Flickr</a> (CC BY-NC-ND 2.0)"
url: "alkoholismus"
libraries: [jquery, highcharts]
styles: ["./styl/charts.css"]
recommended:
  - link: https://interaktivni.rozhlas.cz/rozdelena-spolecnost/
    title: Rozdělená společnost? Čechy proti sobě staví věk a vzdělání
    perex: Loňskou volbu amerického prezidenta i britské referendum o odchodu z Evropské unie ovládli nespokojení voliči, kteří věří na jednoduchá řešení. Překvapení analytici od té doby pátrají, kde se v takovém množství vzali a co chtějí. Bude se totéž opakovat v Česku?
    image: https://interaktivni.rozhlas.cz/rozdelena-spolecnost/media/cover.jpg
  - link: https://interaktivni.rozhlas.cz/umrti-srdce/
    title: Na co se kde v Evropě nejčastěji umírá? Podívejte se na podrobnou mapu
    perex: Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi.
    image: https://interaktivni.rozhlas.cz/umrti-srdce/media/cover.jpg
---

Ženy v posledních dvaceti letech pijí víc alkoholu a častěji pijí nárazově. Víc než dřív se proto stávají jeho oběťmi. Na následky dlouhodobého pití nebo krátkodobého excesu jich dnes umírá dvakrát víc než v polovině devadesátých let. Vyplývá to z dat, která Český rozhlas zpracoval na základě demografických studií.

Až do devadesátých let měli na alkoholem způsobená úmrtí monopol muži. S dlouhodobým pitím se v té době pojila desetina všech mužských úmrtí. Většinou umírali na chronické nemoci způsobené dlouhodobým pitím; dopravní nehody pod vlivem alkoholu nebo otravy tvořily pouze malou část úmrtnosti. Ve stejném období umíralo na následky pití alkoholu pouze jedno procento žen - obvykle právě při nehodách a otravách.

<aside class="big" data-bso="1">
  <div id='chart_umrtnost'></div>
</aside>

To se v posledních dvou dekádách mění. Data z [evropských výběrových šetření o zdraví od roku 1993](http://www.uzis.cz/katalog/mimoradne-publikace/vyberove-setreni-zdravotnim-stavu-ceske-populace-ehis-cr-drive-his-cr) do roku [2015](http://www.uzis.cz/ehis/hlavni-vysledky) prozrazují, že dnes ženy pijí téměř dvojnásobně: z 0,8 standardního nápoje denně (*standardní nápoj* znamená přibližně půllitr piva nebo dvě deci vína) se stalo 1,4 standardního nápoje. Muži ve stejném období pili výrazně víc, ale stabilně. Z 2,6 vyrostlo denní množství standardních nápojů na 2,8.

Nezměnilo se jen množství vypitého alkoholu, ale také zvyky spojené s pitím. Podíl žen, které pijí denně, sice zůstal podobný, ale o dvě třetiny ubylo abstinentek, a naopak přibylo žen, které pijí nárazově - alespoň jednou měsíčně na posezení vypijí tři půllitry nebo tři sklenky vína.

Ženy dohánějí v konzumaci alkoholu muže i [v celosvětovém měřítku](https://www.theguardian.com/society/2016/oct/24/women-drink-alcohol-men-global-study). Posun ve statistikách způsobují jednak úspěšné marketingové kampaně cílené na ženy, jednak rozšíření sladkých alkoholických nápojů. Ceny alkoholu také po celém světě neustále klesají. Podle některých zahraničních studií tak mladé ženy pijí dokonce více než muži.

Kritické období nastává - u mužů i u žen - mezi 35. a 44. rokem života. U mužů se v tomto věku podílí alkohol na 26 procentech všech úmrtí, u žen je to 17 procent. „U alkoholu ročně zaregistrujeme několik stovek úmrtí v důsledku předávkování,“ doplňuje národní protidrogový koordinátor Jindřich Vobořil. „Loni to bylo 342 případů, to je přibližně desetkrát víc než u všech ostatních drog dohromady.“

<aside class="big">
  <div id='chart_vek'></div>
</aside>

Předávkování nebo dopravní nehody pod vlivem ovšem tvoří menší část úmrtí způsobených alkoholem. Nadměrné pití také vyvolává řadu nemocí, ať už samostatně - například cirhózu jater - nebo v kombinaci s dalšími vlivy, jako jsou nemoci srdce nebo zhoubné nádory. Tam, kde není alkohol jediným původcem nemoci, se lékařské studie snaží odhadnout, jakou měrou se na úmrtích podílel. Například na rakovině tlustého střeva se podle lékařů alkohol podílí ze sedmi procent u mužů a ze tří procent u žen. Díky tomu dokážeme odhadnout, za jak velkým podílem na úmrtnosti stojí líh.

Demografka Iva Kohoutová zmíněnou metodou (v angličtině pojmenovanou *alcohol-attributable fractions*) [spočítala české oběti alkoholu](http://casopis.adiktologie.cz/public.magazine/download-file/116). Alkohol se podle ní od roku 1994 stabilně podílí asi na šesti procentech všech úmrtí. Stejné číslo se objevuje i [ve starší analýze](http://www.demografie.info/?cz_detail_clanku&artclID=496) sociologa Karla Vrány, jen rozdíl v alkoholem způsobené úmrtnosti mezi oběma pohlavími u něj není tak radikální.

## Devadesát procent večerek prodá alkohol neplnoletým

Dlouhodobě rostoucí oblibu alkoholu u žen potvrzují také data z protialkoholních léčeben. Zatímco v šedesátých letech připadala v léčbě závislosti na alkoholu na každou ženu asi dvacítka mužů, v osmdesátých letech to bylo pět mužů na jednu ženu a dnes se poměr blíží dvěma ku jedné. Trend vyrovnávání počtu mužů a žen potvrzují také data z ambulancí.

„Je pravda, že se statistiky protialkoholních léčeben vyrovnávají, postupně přibývá žen; třeba v sedmdesátých letech si ale ženy po čtyřicítce nosily \[místo alkoholu\] v kabelce prášky,“ dodává Vobořil.

Data z protialkoholních léčeben upozorňují ještě na jednu ohroženou skupinu: na nezletilé. Je to jediná věková skupina, kde alkohol představuje stejně velký problém pro dívky i chlapce.

„Velký problém u nás představuje velmi snadná dostupnost alkoholu,“ komentuje to Vobořil. „České zákony jsou příliš liberální.“

„Podle vlastních odpovědí si cigarety nebo pivo celkem snadno nebo velmi snadno dokáže obstarat více než 80 procent šestnáctiletých studentů,“ píše se ve [výroční zprávě o stavu drog](http://www.drogy-info.cz/publikace/vyrocni-zpravy/vyrocni-zprava-o-stavu-ve-vecech-drog-v-ceske-republice-v-roce-2015/), kterou vydává Vobořilův úřad. „Polovina studentů by si snadno obstarala i jiný druh alkoholu, konopné látky a léky se sedativním účinkem.“

Stejný dokument popisuje situaci, kdy nezletilí figuranti nakupovali alkohol v supermarketech, restauracích a večerkách. V supermarketech a restauracích uspěli v polovině případů, u večerek byla úspěšnost devadesátiprocentní.

Problematické pití neplnoletých podtrhuje nejnovější [evropské výběrové šetření o zdraví z roku 2014](http://www.uzis.cz/node/7495). To tvrdí, že nárazovému pití většího množství alkoholu se nejméně jednou měsíčně oddává 17 procent žen a 38 procent mužů ve věku 15-24 let.

<aside class="big">
  <div id='chart_riziko'></div>
</aside>